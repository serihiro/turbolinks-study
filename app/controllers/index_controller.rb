class IndexController < ApplicationController
  def index
    @content = 'index'
  end

  def other
    @content = 'other'
  end
end

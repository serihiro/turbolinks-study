Rails.application.routes.draw do
  root to: 'index#index'
  get '/other' => 'index#other' , as: :other
end
